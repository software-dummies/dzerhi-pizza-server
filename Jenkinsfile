pipeline {
    agent any

    tools {
        jdk 'EclipseTemurin-17'
    }

    stages {
        stage('Build And Test') {
            steps {
                echo 'Running build with generating code analysis reports'
                withGradle {
                    sh './gradlew clean build'
                }
                stash includes: '**/build/**', name: 'build'
            }
        }
        stage('Generate Test Coverage report and update it with codacy') {
            steps {
                echo 'Generating jacoco Test Report'
                withGradle {
                    sh './gradlew jacocoTestReport'
                }
                echo 'Publishing test results'
                junit '**build/test-results/test/TEST-*.xml'
                echo 'Running Codacy script'
                sh 'bash <(curl -Ls https://coverage.codacy.com/get.sh) report -r build/reports/jacoco/test/jacocoTestReport.xml'
                unstash name: 'build'
            }
        }
        stage('Update Codacy static code analysis reports') {
            steps {
                echo 'Running codacy docker tool'
                sh 'codacy-analysis-cli analyze --tool detekt --max-allowed-issues 100 --upload'
            }
        }
    }
    post {
        always {
            archiveArtifacts artifacts: '**/reports/**/detekt.*', onlyIfSuccessful: false
        }
    }
}