import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val springVersion = "3.0.1"
val kotlinVersion = "1.7.21"
val excludedSources = setOf<String>()

plugins {
    kotlin("jvm") version "1.7.21"
    application
    id("io.gitlab.arturbosch.detekt") version "1.22.0"
    id("org.springframework.boot") version "3.0.1"
    jacoco
    id("org.jetbrains.kotlin.plugin.allopen") version "1.7.21"
    kotlin("plugin.spring") version "1.7.21"
    kotlin("plugin.jpa") version "1.7.21"
    kotlin("kapt") version "1.7.21"
}

allOpen {
    annotation("jakarta.persistence.Entity")
    annotation("org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest")
}

jacoco {
    toolVersion = "0.8.8"
    applyTo(tasks.run.get())
}

detekt {
    toolVersion = "1.19.0"
    config = files("detekt.yml")
    buildUponDefaultConfig = true
    ignoreFailures = true
}

group = "ru.software.dummies"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springVersion")
    implementation("org.springframework.boot:spring-boot-starter-web:$springVersion")
    runtimeOnly("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.liquibase:liquibase-core:4.18.0")
    implementation("org.postgresql:postgresql:42.5.1")
    runtimeOnly("com.h2database:h2:2.1.214")

    testImplementation(kotlin("test"))
    testImplementation("org.springframework.boot:spring-boot-starter-test:$springVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
    testImplementation("io.mockk:mockk:1.13.4")

    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.22.0")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnitPlatform()
    extensions.configure(JacocoTaskExtension::class) {
        setDestinationFile(layout.buildDirectory.file("jacoco/jacocoTest.exec").get().asFile)
        classDumpDir = layout.buildDirectory.dir("jacoco/classpathdumps").get().asFile
    }
    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)

    reports {
        html.required.set(true)
        xml.required.set(true)
    }

    classDirectories.setFrom(classDirectories.asFileTree.matching { setExcludes(excludedSources) }.files)
}

tasks.jacocoTestCoverageVerification {
    dependsOn(tasks.jacocoTestReport)
}

tasks.clean {
    if (project.hasProperty("updateRes")) {
        sync {
            from("config/idea/runConfigurations").into(".idea/runConfigurations")
        }
    }
}

tasks.withType<Detekt>().configureEach {
    reports {
        xml.required.set(false)
        html.required.set(true)
        sarif.required.set(false)
        md.required.set(true)
        txt.required.set(true)
    }
    exclude("**/resources/**", "**/build/**", "**/test/**")
}

tasks.withType<DetektCreateBaselineTask>().configureEach() {
    exclude("**/resources/**", "**/build/**", "**/test/**")
}

tasks.withType<ProcessResources>() {
    duplicatesStrategy = org.gradle.api.file.DuplicatesStrategy.EXCLUDE
}

tasks.register("bootRunProfile") {
    group = "application"
    description = "Runs Spring Boot application with profile"
    doLast {

        tasks.bootRun.configure {
            val profile: String =
                if (project.hasProperty("profile")) project.properties["profile"] as String
                else "dev"
            systemProperty("spring.profiles.active", profile)
        }
    }
    finalizedBy("bootRun")
}

tasks.register<JacocoReport>("applicationCodeCoverageReport") {
    executionData(tasks.run.get())
    sourceSets(sourceSets.main.get())
}

application {
    mainClass.set("ru.software.dummies.MainKt")
}

val compileKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks

compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
