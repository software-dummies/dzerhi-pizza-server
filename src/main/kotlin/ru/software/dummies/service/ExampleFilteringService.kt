package ru.software.dummies.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.software.dummies.entity.ExampleEntity
import ru.software.dummies.repository.ExampleRepository

@Service
class ExampleFilteringService @Autowired constructor(
    private val exampleRepository: ExampleRepository
) {
    fun filterByOddIds(): Array<ExampleEntity> {
        val entities = exampleRepository.findAll()

        return entities.filter { it.id % 2L == 0L }.toTypedArray()
    }
}