package ru.software.dummies.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.software.dummies.entity.ExampleEntity

@Repository
interface ExampleRepository : CrudRepository<ExampleEntity, Long> {
}