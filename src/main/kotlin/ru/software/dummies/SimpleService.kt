package ru.software.dummies

class SimpleService {

    fun simpleMethod(boolean: Boolean?): Int {
        return when {
            boolean == null -> -1
            boolean -> 1
            else -> 0
        }
    }

    fun uncoveredMethod() {
        println("foo")
    }
}
