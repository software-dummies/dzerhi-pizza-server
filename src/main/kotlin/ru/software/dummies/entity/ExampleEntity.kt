package ru.software.dummies.entity

import jakarta.persistence.*

@Entity
@Table(name = "example")
class ExampleEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0,
    @Column(name = "name", nullable = false)
    var name: String
) {
    override fun toString(): String {
        return "ExampleEntity=[id=$id, name=$name];";
    }
}
