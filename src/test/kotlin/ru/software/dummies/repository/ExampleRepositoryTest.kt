package ru.software.dummies.repository

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import ru.software.dummies.entity.ExampleEntity

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ExampleRepositoryTest @Autowired constructor(
    private val entityManager: TestEntityManager,
    private val repository: ExampleRepository
){
    @Test
    fun `example save test`() {
        val someUsers = listOf<ExampleEntity>(
            ExampleEntity(name = "John"),
            ExampleEntity(name = "Andrew"),
            ExampleEntity(name = "Kate")
        )
        someUsers.forEach { entityManager.persist(it) }
        entityManager.flush()

        val usersFromDb = repository.findAll()

        usersFromDb.forEach(::println)

        Assertions.assertEquals(someUsers.size, usersFromDb.count())
    }
}