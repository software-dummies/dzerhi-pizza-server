package ru.software.dummies.service

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.extension.ExtendWith
import ru.software.dummies.entity.ExampleEntity
import ru.software.dummies.repository.ExampleRepository

@ExtendWith(MockKExtension::class)
class ExampleFilteringServiceTest {
    private val oddEntities = listOf(
        ExampleEntity(2, "Ann"),
        ExampleEntity(4, "Hannah")
    )

    private val exampleRepository: ExampleRepository = mockk();

    private val filteringService = ExampleFilteringService(exampleRepository)

    @Test
    fun `filter does got just odd values`() {
        val allEntities = mutableListOf(
            ExampleEntity(1, "Josh"),
            ExampleEntity(3, "Sharif")
        )
        allEntities.addAll(oddEntities)

        every { exampleRepository.findAll() } returns allEntities

        val result = filteringService.filterByOddIds().asList()

        Assertions.assertEquals(oddEntities, result)
    }
}